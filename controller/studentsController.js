const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

var { Students } = require('../model/students');
var { Books } = require('../model/books');
var { Transactions } = require('../model/transactions');

RegExp.quote = function (str) {
    return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
};

// /students (find All)
router.get('/', (req, res) => {
    if (!req.query.src) {
        Students.find({}, 'name birthDate updatedDate').sort({ createdDate: -1 }).exec(
            (err, docs) => {
                if (err) {
                    console.log(`Error retrieving data: ${JSON.stringify(err, undefined, 2)}`);
                    res.status(500).send(err);
                }
                else res.status(200).send(docs);
            }
        );
    }
    else {
        let regex = new RegExp(RegExp.quote(req.query.src), 'i');
        Students.find({ $or: [{ name: regex }] }, 'name birthDate updatedDate').sort({ createdDate: -1 }).exec(
            (err, docs) => {
                if (err) {
                    console.log(`Error retrieving data: ${JSON.stringify(err, undefined, 2)}`);
                    res.status(500).send(err);
                }
                else res.status(200).send(docs);
            });
    }
});

// /students/:docId (find One)
router.get('/:docId', (req, res) => {
    if (!ObjectId.isValid(req.params.docId))
        return res.status(400).send(`Document Id is not valid.`);

    Students.findById(req.params.docId).populate(
        "borrows.bookId", "title"
    ).populate("borrows.transId", "transDate").exec((err, doc) => {
        if (!err) { res.send(doc); }
        else {
            console.log('Error retrieving data :' + JSON.stringify(err, undefined, 2));
            res.status(500).send(err);
        }
    });
});

// /students/ (POST - add new student)
router.post('/', (req, res) => {
    var student = new Students({
        name: req.body.name,
        birthDate: req.body.birthDate,
    });
    student.save((err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Adding Student :' + JSON.stringify(err, undefined, 2)); }
    });
});

// -> /students/:docId (PUT - for update student data)
router.put('/:docId', (req, res) => {
    if (!ObjectId.isValid(req.params.docId))
        return res.status(400).send(`No record with given id : ${req.params.docId}`);

    var student = {
        name: req.body.name,
        birthDate: req.body.birthDate,
        updatedDate: new Date()
    };
    Students.findByIdAndUpdate(req.params.docId, { $set: student }, { new: true }, (err, doc) => {
        if (!err) { res.send(doc); }
        else {
            console.log('Error in Update Student Data :' + JSON.stringify(err, undefined, 2));
            res.status(500).send(err);
        }
    });
});

// -> /students/:docId (DELETE - for delete)
router.delete('/:docId', (req, res) => {
    if (!ObjectId.isValid(req.params.docId))
        return res.status(400).send(`No record with given id : ${req.params.docId}`);

    Students.findByIdAndRemove(req.params.docId, (err, doc) => {
        if (!err) {
            res.send(doc);
        }
        else {
            console.log('Error in Delete Student Data: ' + JSON.stringify(err, undefined, 2));
            res.status(500).send(err);
        }
    });
});

// -> /students/borrow (POST form? for borrow)
router.post('/borrow', (req, res) => {
    if (!ObjectId.isValid(req.body.userId) || !ObjectId.isValid(req.body.bookId))
        return res.status(400).send(`Bad request`);

    Books.findById(req.body.bookId, (err, doc) => {
        if (!err) {
            if (doc) {
                if (doc.stock > 0) {
                    Students.findById(req.body.userId, (err, doc) => {
                        if (!err) {
                            if (doc) {
                                if (doc.borrows.length == 0) {
                                    Books.findByIdAndUpdate(req.body.bookId, { $inc: { stock: -1 } }, (err, doc) => {
                                        if (!err) {
                                            let tran = new Transactions({
                                                studentId: req.body.userId,
                                                bookId: req.body.bookId,
                                                transType: 'borrow',
                                            });
                                            tran.save((err, doc) => {
                                                Students.findByIdAndUpdate(req.body.userId,
                                                    { $push: { borrows: { bookId: req.body.bookId, transId: doc._id } } }, (err, doc) => {
                                                        if (!err) {
                                                            return res.send(`Borrow success.`);
                                                        }
                                                        else {
                                                            return res.status(500).send(err);
                                                        }
                                                    });
                                            })
                                        }
                                        else {
                                            return res.status(500).send(err);
                                        }
                                    })
                                }
                                else {
                                    doc.borrows.forEach((elem, idx, array) => {
                                        if (elem.bookId == req.body.bookId) {
                                            return res.send(`You're currently borrowing the book.`);
                                        }
                                        else {
                                            if (idx == array.length - 1) {
                                                Books.findByIdAndUpdate(req.body.bookId, { $inc: { stock: -1 } }, (err, doc) => {
                                                    if (!err) {
                                                        let tran = new Transactions({
                                                            studentId: req.body.userId,
                                                            bookId: req.body.bookId,
                                                            transType: 'borrow',
                                                        });
                                                        tran.save((err, doc) => {
                                                            Students.findByIdAndUpdate(req.body.userId,
                                                                { $push: { borrows: { bookId: req.body.bookId, transId: doc._id } } }, (err, doc) => {
                                                                    if (!err) {
                                                                        return res.send(`Borrow success.`);
                                                                    }
                                                                    else {
                                                                        return res.status(500).send(err);
                                                                    }
                                                                });
                                                        })
                                                    }
                                                    else {
                                                        return res.status(500).send(err);
                                                    }
                                                })
                                            }
                                        }
                                    })
                                }
                            }
                            else {
                                return res.send("User not found.");
                            }
                        }
                        else {
                            return res.status(500).send(err);
                        }
                    });
                }
                else {
                    return res.send(`Book stock depleted.`);
                }
            }
            else {
                return res.send(`Book not found.`);
            }
        }
        else {
            return res.status(500).send(err);
        }
    })
})

// -> /students/return (POST form? for return)
router.post('/return', (req, res) => {
    if (!ObjectId.isValid(req.body.userId) || !ObjectId.isValid(req.body.bookId))
        return res.status(400).send(`Bad request`);

    Students.findById(req.body.userId, (err, doc) => {
        if (!err) {
            if (doc) {
                if (doc.borrows.length == 0) return res.send(`You are not borrowing this book.`);
                else {
                    doc.borrows.forEach((elem, idx, array) => {
                        if (elem.bookId == req.body.bookId) {
                            Books.findById(req.body.bookId, (err, doc) => {
                                if (!err) {
                                    if (doc) {
                                        Books.findByIdAndUpdate(req.body.bookId, { $inc: { stock: 1 } }, (err, doc) => {
                                            if (!err) {
                                                let tran = new Transactions({
                                                    studentId: req.body.userId,
                                                    bookId: req.body.bookId,
                                                    transType: 'return',
                                                });
                                                tran.save((err, doc) => {
                                                    if (!err) {
                                                        Students.findByIdAndUpdate(req.body.userId, { $pull: { borrows: elem } },
                                                            (err, doc) => {
                                                                if (!err) {
                                                                    return res.send("Successfully returned.");
                                                                }
                                                                else {
                                                                    return res.status(500).send(err);
                                                                }
                                                            })
                                                    }
                                                    else {
                                                        return res.status(500).send(err);
                                                    }
                                                })
                                            }
                                            else {
                                                return res.status(500).send(err);
                                            }
                                        });
                                    }
                                    else {
                                        return res.send("Book not found.");
                                    }
                                }
                                else {
                                    return res.status(500).send(err);
                                }
                            })
                        }
                        else {
                            if (idx == array.length - 1) {
                                return res.send(`You are not borrowing this book.`);
                            }
                        }
                    })
                }
            }
            else {
                return res.send(`User not found.`);
            }
        }
        else {
            return res.status(500).send(`Internal server error.`);
        }
    })
})

module.exports = router;