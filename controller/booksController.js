const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

var { Books } = require('../model/books');

RegExp.quote = function (str) {
    return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
};

// /books (find All)
router.get('/', (req, res) => {
    if (!req.query.src) {
        Books.find({}, 'title genre author publishYear updatedDate').sort({ createdDate: -1 }).exec(
            (err, docs) => {
                if (err) {
                    console.log(`Error retrieving data: ${JSON.stringify(err, undefined, 2)}`);
                    res.status(500).send(err);
                }
                else res.status(200).send(docs);
            }
        );
    }
    else {
        let regex = new RegExp(RegExp.quote(req.query.src), 'i');
        Books.find({ $or: [{ title: regex }, { genre: regex }, { author: regex }] }, 
            'title genre author publishYear updatedDate').sort({ createdDate: -1 }).exec(
            (err, docs) => {
                if (err) {
                    console.log(`Error retrieving data: ${JSON.stringify(err, undefined, 2)}`);
                    res.status(500).send(err);
                }
                else res.status(200).send(docs);
            });
    }
});

// /books/:docId (find One)
router.get('/:docId', (req, res) => {
    if (!ObjectId.isValid(req.params.docId))
        return res.status(400).send(`No record with given id : ${req.params.docId}`);

    Books.findById(req.params.docId, (err, doc) => {
        if (!err) { res.send(doc); }
        else {
            console.log('Error retrieving data :' + JSON.stringify(err, undefined, 2));
            res.status(500).send(err);
        }
    });
});

// /books/ (POST - add new book)
router.post('/', (req, res) => {
    var book = new Books({
        title: req.body.title,
        author: req.body.author,
        genre: req.body.genre,
        stock: req.body.stock,
        pageCount: req.body.pageCount,
        publishYear: req.body.publishYear
    });
    book.save((err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Adding Bool :' + JSON.stringify(err, undefined, 2)); }
    });
});

// -> /books/:docId (PUT - for update book data)
router.put('/:docId', (req, res) => {
    if (!ObjectId.isValid(req.params.docId))
        return res.status(400).send(`No record with given id : ${req.params.docId}`);

    var book = {
        title: req.body.title,
        author: req.body.author,
        genre: req.body.genre,
        pageCount: req.body.pageCount,
        stock: req.body.stock,
        publishYear: req.body.publishYear,
        updatedDate: new Date()
    };
    Books.findByIdAndUpdate(req.params.docId, { $set: book }, { new: true }, (err, doc) => {
        if (!err) { res.send(doc); }
        else {
            console.log('Error in Update Book Data :' + JSON.stringify(err, undefined, 2));
            res.status(500).send(err);
        }
    });
});

// -> /books/:docId (DELETE - for delete)
router.delete('/:docId', (req, res) => {
    if (!ObjectId.isValid(req.params.docId))
        return res.status(400).send(`No record with given id : ${req.params.docId}`);

    Books.findByIdAndRemove(req.params.docId, (err, doc) => {
        if (!err) {
            res.send(doc);
        }
        else {
            console.log('Error in Delete Student Data: ' + JSON.stringify(err, undefined, 2));
            res.status(500).send(err);
        }
    });
});

module.exports = router;