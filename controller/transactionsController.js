const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

var { Transactions } = require('../model/transactions');

// /trans/:userId
router.get('/:userId', (req, res) => {
    Transactions.find({ studentId: req.params.userId }).populate("bookId").sort({ transDate: -1 }).exec((err, docs) => {
        if (!err)
            res.send(docs);
        else {
            res.status(500).send(err);
        }
    })
});

module.exports = router;