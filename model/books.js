const mongoose = require('mongoose');

var Books = mongoose.model("Books", {
    title: { type: String },
    pageCount: { type: Number },
    genre: { type: String },
    author: { type: String },
    publishYear: { type: Number },
    stock: { type: Number, default: 1 },
    createdDate: { type: Date, default: Date.now },
    updatedDate: { type: Date, default: Date.now },
});

module.exports = { Books };