const mongoose = require('mongoose');

var Transactions = mongoose.model("Transactions", {
    studentId: { type: mongoose.Schema.Types.ObjectId, ref: "Students" },
    bookId: { type: mongoose.Schema.Types.ObjectId, ref: "Books" },
    transType: { type: String },
    transDate: { type: Date, default: Date.now },
});

module.exports = { Transactions };