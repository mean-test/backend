const mongoose = require('mongoose');

var Students = mongoose.model("Students", {
    name: { type: String },
    birthDate: { type: Date },
    createdDate: { type: Date, default: Date.now },
    updatedDate: { type: Date, default: Date.now },
    borrows: [{
        bookId: { type: mongoose.Schema.Types.ObjectId, ref: "Books" },
        transId: { type: mongoose.Schema.Types.ObjectId, ref: "Transactions" }
    }]
});

module.exports = { Students };