const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const { mongoose } = require('./db');
var studentsController = require('./controller/studentsController');
var booksController = require('./controller/booksController');
var transController = require('./controller/transactionsController');

var app = express();
app.use(bodyParser.json());

//untuk angular (frontend)
app.use(cors({ origin: 'http://localhost:4200' }));

//buka server di port...
app.listen(2121, () => console.log(`Server started at port 2121`));

//buka link routing ke...
app.use('/students', studentsController);
app.use('/books', booksController);
app.use('/trans', transController);
