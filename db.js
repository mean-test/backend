const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/meantest', (err) => {
    if (err) {
        console.log(`Connection error. ${err}`);
    }
    else console.log(`Connection established`);
})


module.exports = mongoose;